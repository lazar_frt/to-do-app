import React from 'react'
import styled from 'styled-components'


const DeleteButton = styled.a`
cursor: pointer;
`
function Buttons() {
    return (
        <div>
            <DeleteButton>Delete</DeleteButton>
            <DeleteButton>Edit</DeleteButton>
        </div>
    );
}

export default Buttons;
