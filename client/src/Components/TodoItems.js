import React, { useState } from 'react'
import styled from 'styled-components'
import ListItem from './ListItem'

const Container = styled.div`
    width: 409px;
    height: 240px;
margin: 14px 58px 92px 0;
box-shadow: 0 2px 7px -2px rgba(0, 0, 0, 0.15);
background-color: #ffffff;
position: relative;
`

const Button = styled.button`
position: absolute;
bottom: -20px;
left: 135px;
background-color: #af7eeb;
border-radius: 28px;
border: 0;
display: inline-block;
cursor: pointer;
color: #ffffff;
font-family: Arial;
font-size: 14px;
padding: 12px 31px;
-webkit-text-decoration: none;
text-decoration: none;
text-shadow: 0px 1px 0px #2f6627;
height: 44px;
font-family: OpenSans;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
  :active {
    bottom:-19px;
}
`

const UList = styled.ul`
padding:40px;
`

const todos = ['first task', 'second task', 'third task']

function TodoItems() {
    const [selected, setSelected] = useState(false)

    return (
        <Container>
            <UList>
                {todos.map((item, index) =>
                    <ListItem index={index} tittle={item} />
                )}
            </UList>
            <Button>+ New task</Button>
        </Container>
    );
}

export default TodoItems;
