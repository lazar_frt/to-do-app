import React, { useState } from 'react'
import styled from 'styled-components'
import { IoReorderThreeOutline } from "react-icons/io5"
import TodoItems from './TodoItems'

const Header = styled.nav`
width: 409px;
height: 58px;
box-shadow: 0 2px 7px -2px rgba(0, 0, 0, 0.15);
background-color: #af7eeb;
position: relative;
`

const ToggleButton = styled.a`
position: absolute;
    left: 20px;
    top: 11px;
    color:white;
    cursor: pointer;
:active {
    top:13px;
}
`

const Typography = styled.div`
height: 22px;
font-family: OpenSans;
font-size: 16px;
font-weight: bold;
font-stretch: normal;
font-style: normal;
line-height: normal;
-webkit-letter-spacing: normal;
-moz-letter-spacing: normal;
-ms-letter-spacing: normal;
letter-spacing: normal;
color: #ffffff;
padding-top: 20px;
`

function Container() {
    const [show, setShow] = useState(false);

    const onClick = () => {
        !show ? setShow(true) : setShow(false);
    }

    return (
        <div className="App">
            <Header>
                <ToggleButton onClick={onClick}><IoReorderThreeOutline className="buttonIcon" /></ToggleButton>
                <Typography>
                    Motoff To-do List
                </Typography>
            </Header>
            {show && <TodoItems />}
        </div>
    );
}

export default Container;
