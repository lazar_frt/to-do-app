import React, { useState } from 'react'
import styled from 'styled-components'
import Buttons from './Buttons'

const Label = styled.label`
margin-left:20px;
margin-right:90px;
`

const Frame = styled.div`
    display:flex;
`

function ListItem({tittle}) {
    const [selected, setSelected] = useState(false)
    const onSelected = () => {
       setSelected(true)
    }

    return (
        <Frame>
            <input type="radio" name="fav_language" value="HTML" onChange={onSelected} />
            <Label for="html">{tittle}</Label>
            {selected && <Buttons />}
        </Frame>
    );
}

export default ListItem;
